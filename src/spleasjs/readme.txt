Non-ActionEvents used:
    -KeyEvent: used to determine if the ctrl button is being held down
    -ScrollEvent: used to scale the image when the ctrl button is being held and the user is scrolling
    -WorkerEvent: used for onTaskCompleted to clean up some threaded things
                  used for onTaskFailed to detect what exception happened and respond appropriately

Non-required things implemented:
    -Controls are instead organized in menus instead of buttons
    -Zoom functionality is added, user can either scroll, or use several buttons to zoom to specific ratios
        -Image is in a ScrollPane, allowing the user to pan around the image when zoomed in
        -Background has a pattern, allows transparency to be easier to notice
    -Kernel was implemented manually, allows for 5x5 kernels and a few other things
        -Different edge behavior can be selected
        -Additional example kernels can be selected
        -The input cells can only contain numeric values
        -3x3 kernels can still be used if the outer cells are 0
    -Different things were implemented as JavaFX tasks
        -Is a interface for threading that allows further control of threads in an FX environment
        -Prevents the user interface from freezing during longer processes like kernel application
            or .msoe loading/saving
        -Allows from a progress bar in the main window
        -Can be cancelled early to prevent the process from being completed, no files are modified
            if it is cancelled.
