/*
 * Course: SE-1021
 * Term: Winter 2017-2018
 * Assignment: Final Lab
 * Author: Joshua Spleas
 * Date: 2/6/2018
 */
package spleasjs;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * Main class for an image manipulation program. This class contains the initialization information
 * for the two main windows.
 */
public class Main extends Application {
    /**
     * Default width for the main window in pixels
     */
    private static final int DEFAULT_MAIN_WIDTH = 800;
    /**
     * Default height for the main window in pixels
     */
    private static final int DEFAULT_MAIN_HEIGHT = 475;
    /**
     * Minimum width for the main window in pixels
     */
    private static final int MIN_MAIN_WIDTH = 560;
    /**
     * Minimum height for the main window in pixels
     */
    private static final int MIN_MAIN_HEIGHT = 300;

    /**
     * Main initialization method for the user interface, loads the fxmls for the two windows
     * @param primaryStage root stage for the interface
     * @throws Exception required by override
     */
    @Override
    public void start(Stage primaryStage) throws Exception{
        FXMLLoader rootLoader = new FXMLLoader();
        Parent root = rootLoader.load(getClass().getResource("main.fxml").openStream());
        primaryStage.setTitle("Image Manipulator");
        primaryStage.setScene(new Scene(root, DEFAULT_MAIN_WIDTH, DEFAULT_MAIN_HEIGHT));
        primaryStage.setMinWidth(MIN_MAIN_WIDTH);
        primaryStage.setMinHeight(MIN_MAIN_HEIGHT);
        primaryStage.show();

        MainController mainController = rootLoader.getController();
        mainController.initializeSceneDependent();
    }

    /**
     * Main entrypoint for the application
     * @param args passes to launch
     */
    public static void main(String[] args) {
        launch(args);
    }
}
