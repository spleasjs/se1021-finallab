/*
 * Course: SE-1021
 * Term: Winter 2017-2018
 * Assignment: Final Lab
 * Author: Joshua Spleas
 * Date: 2/6/2018
 */
package spleasjs;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.PixelReader;
import javafx.scene.paint.Color;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.io.DataOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.Scanner;

/**
 * Class containing only static methods used to read and write standard, msoe, and bmsoe image files
 */
public class ImageIO {

    private static final int ONE_BYTE_MAX = 255;
    private static final int TWO_BYTE_BITS = 16;
    private static final int ONE_BYTE_BITS = 8;
    private static final int THREE_BYTES_BITS = 24;

    /**
     * Reads the file given and returns an image. If the type is .msoe or .bmsoe, pass the file to
     * the respective method
     * @param file file to load
     * @param imageContainer imageView to read the new image into
     * @throws IOException when failed to read due to file IO
     * @return a task that is reading the image, or null if image is a standard type and
     *          multithreading is unnecessary
     */
    public static Task<Void> read(File file, ImageView imageContainer) throws IOException {
        if (file == null) {
            throw new NullPointerException("File given was null");
        }
        if (imageContainer == null) {
            throw new NullPointerException("ImageView given was null");
        }
        Task<Void> readTask = null;

        FileReader fileDeterminer = new FileReader(file);
        char[] firstFive = new char[] {0, 0, 0, 0, 0};
        fileDeterminer.read(firstFive, 0, firstFive.length);
        fileDeterminer.close();
        String firstFiveString = new String(firstFive);
        firstFiveString = firstFiveString.toLowerCase();
        if (firstFiveString.equals("bmsoe")) {
            readTask = readBMSOE(file, imageContainer);
        } else if (firstFiveString.matches("msoe\\s")) {
            readTask = readMSOE(file, imageContainer);
        } else {
            BufferedImage bufferedImage = javax.imageio.ImageIO.read(file);
            if(bufferedImage != null) {
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                imageContainer.setImage(image);
            } else{
                throw new IllegalArgumentException("File: " + file.getPath() + " not a valid type");
            }
        }

        return readTask;
    }

    /**
     * Writes an image to a file. resulting file can be either a standard image, a .msoe image, or
     * a .bmsoe image.
     * @param image image to get pixel data from
     * @param file file to write to
     * @throws IOException when error in writing
     * @return a task that will write the image to a file
     */
    public static Task<Void> write(Image image, File file) throws IOException {
        if (file == null || image == null) {
            throw new NullPointerException("Null pointer given in write!");
        }
        Task<Void> task = null;
        String filePath = file.getPath();
        String extension = filePath.substring(filePath.lastIndexOf('.') + 1);
        extension = extension.toLowerCase();

        switch (extension) {
            case "bmsoe":
                task = writeBMSOE(image, file);
                break;
            case "msoe":
                task = writeMSOE(image, file);
                break;
            default:
                boolean wrote = javax.imageio.ImageIO.write(SwingFXUtils.fromFXImage(image,
                        null), extension, file);
                if (!wrote) {
                    throw new
                            IllegalArgumentException("Unable to write, file tpye likely invalid");
                }
                break;
        }
        return task;
    }

    /**
     * Reads a .msoe file. If the file is not properly formatted, it tries again ignoring newlines
     * @param file file to load
     * @param imageContainer imageView to read the new image into
     * @return a task that is reading the image
     */
    private static Task<Void> readMSOE(File file, ImageView imageContainer){
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {

                Scanner imageIn = null;
                try {
                    imageIn = new Scanner(file);
                    imageIn.nextLine();
                    String sizeString = imageIn.nextLine();
                    Scanner sizeScanner = new Scanner(sizeString);
                    int width = sizeScanner.nextInt();
                    int height = sizeScanner.nextInt();
                    WritableImage image = new WritableImage(width, height);
                    PixelWriter pixelWriter = image.getPixelWriter();

                    for (int y = 0; y < height && !isCancelled(); ++y) {
                        String row = imageIn.nextLine();
                        Scanner rowScanner = new Scanner(row);
                        for (int x = 0; x < width && !isCancelled(); ++x) {
                            pixelWriter.setColor(x, y, Color.web(rowScanner.next()));
                            updateProgress(y * width + x, width * height);
                        }
                    }
                    if (!isCancelled()) {
                        Platform.runLater(() -> imageContainer.setImage(image));
                    }
                } finally {
                    if (imageIn != null) {
                        imageIn.close();
                    }
                }
                return null;
            }
        };
    }

    /**
     * Writes to a file in the .msoe file format
     * @param image image file to write
     * @param file file to output to
     * @throws IOException when failed to write
     * @return new task that will write to a msoe file
     */
    private static Task<Void> writeMSOE(Image image, File file) throws IOException{
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                File temp = new File("tmp.msoe");
                PrintWriter fileWriter = new PrintWriter(temp);
                fileWriter.println("MSOE");
                fileWriter.println("" + (int)image.getWidth() + " " + (int)image.getHeight());
                PixelReader pixelReader = image.getPixelReader();
                int height = (int)image.getHeight();
                int width = (int)image.getWidth();
                for (int y = 0; y < height && !isCancelled(); ++y) {
                    for (int x = 0; x < width && !isCancelled(); ++x) {
                        Color color = pixelReader.getColor(x, y);
                        fileWriter.print(String.format("#%02X%02X%02X%02X ",
                                (int)(color.getRed() * ONE_BYTE_MAX),
                                (int)(color.getGreen() * ONE_BYTE_MAX),
                                (int)(color.getBlue() * ONE_BYTE_MAX),
                                (int)(color.getOpacity() * ONE_BYTE_MAX)));
                        updateProgress(y * width + x, width * height);
                    }
                    fileWriter.print('\n');
                }
                if(!isCancelled()) {
                    file.delete();
                    fileWriter.close();
                    temp.renameTo(file);
                } else{
                    fileWriter.close();
                    temp.delete();
                }
                return null;
            }
        };
    }

    /**
     * Reads a .bmsoe file
     * @param file file to load
     * @param imageContainer imageView to read the new image into
     * @throws IOException when failed to read due to file error
     * @return a task that is reading the image
     */
    private static Task<Void> readBMSOE(File file, ImageView imageContainer) throws IOException{
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                WritableImage image;
                DataInputStream dataInput = null;
                try {
                    dataInput = new DataInputStream(
                            new BufferedInputStream(new FileInputStream(file)));
                    dataInput.skipBytes("BMSOE".length());
                    int width = dataInput.readInt();
                    int height = dataInput.readInt();
                    image = new WritableImage(width, height);
                    PixelWriter pixelWriter = image.getPixelWriter();

                    int paddingPerRow = width % 4 == 0 ? 0 :
                            TWO_BYTE_BITS - ((width * 4) % TWO_BYTE_BITS);
                    for (int row = 0; row < height && !isCancelled(); ++row) {
                        for (int column = 0; column < width && !isCancelled(); ++column) {
                            pixelWriter.setArgb(column, row,
                                    (dataInput.read() << TWO_BYTE_BITS) +
                                            (dataInput.read() << ONE_BYTE_BITS) +
                                            (dataInput.read()) +
                                            (dataInput.read()<< THREE_BYTES_BITS));
                            updateProgress(row * width + column, width * height);
                        }
                        dataInput.skipBytes(paddingPerRow);
                    }
                    if (!isCancelled()) {
                        Platform.runLater(() -> imageContainer.setImage(image));
                    }
                } finally {
                    if (dataInput != null) {
                        dataInput.close();
                    }
                }
                return null;
            }
        };
    }

    /**
     * Writes to a .bmsoe file
     * @param image image file to read from
     * @param file bmsoe file to write to
     * @return new task that will write to a .bmsoe file when completed
     */
    private static Task<Void> writeBMSOE(Image image, File file){
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                DataOutputStream outputStream = null;
                try{
                    File temp = new File("temp.bmsoe");
                    outputStream = new DataOutputStream(
                            new BufferedOutputStream(new FileOutputStream(temp)));
                    outputStream.writeBytes("BMSOE");
                    int width = (int)image.getWidth();
                    int height = (int)image.getHeight();
                    outputStream.writeInt(width);
                    outputStream.writeInt(height);

                    PixelReader reader = image.getPixelReader();
                    int paddingPerRow = width % 4 == 0 ? 0 :
                            TWO_BYTE_BITS - ((width * 4) % TWO_BYTE_BITS);
                    for (int row = 0; row < height && !isCancelled(); ++row) {
                        for (int column = 0; column < width && !isCancelled(); ++column) {
                            int argbColor = reader.getArgb(column, row);
                            outputStream.writeInt((argbColor<< ONE_BYTE_BITS) |
                                    (argbColor >>> THREE_BYTES_BITS));
                            updateProgress(row * width + column, width * height);
                        }
                        outputStream.write(new byte[paddingPerRow]);
                    }
                    if(!isCancelled()){
                        file.delete();
                        outputStream.close();
                        temp.renameTo(file);
                    } else{
                        temp.delete();
                    }
                } finally{
                    if(outputStream != null){
                        outputStream.close();
                    }
                }
                return null;
            }
        };
    }
}
