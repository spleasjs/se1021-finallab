/*
 * Course: SE1021
 * Term: Winter 2017-2018
 * Assignment: Final Lab
 * Author: Joshua Spleas
 * Date: 2/6/2018
 */

package spleasjs;

import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class to control the FXML for the filter window.
 * Allows the user to specify a 5x5 kernel to apply to the image
 */
public class FilterController implements Initializable {

    //Kernel size, must be odd. If greater than 5, fxml modifications are necessary
    private static final int KERNEL_SIZE = 5;
    
    private static final double IDENTITY_KERNEL[][] = new double[][] { {1.0} };
    private static final double BLUR_KERNEL[][] = new double[][] {
            {0, 1, 0},
            {1, 5, 1},
            {0, 1, 0}
    };
    private static final double SHARPEN_KERNEL[][] = new double[][] {
            { 0, -1, 0},
            {-1, 5, -1},
            { 0, -1, 0}
    };
    private static final double EDGE_KERNEL[][] = new double[][] {
            { 0, -1, 0},
            {-1, 4, -1},
            { 0, -1, 0}
    };
    private static final double GAUSSIAN_3X3_KERNEL[][] = new double[][] {
            { 1, 2, 1},
            { 2, 4, 2},
            { 1, 2, 1}
    };
    private static final double GAUSSIAN_5X5_KERNEL[][] = new double[][] {
            {1, 4, 6, 4, 1},
            {4, 16, 24, 16, 4},
            {6, 24, 36, 24, 6},
            {4, 16, 24, 16, 4},
            {1, 4, 6, 4, 1}
    };
    private static final double EMBOSS_KERNEL[][] = new double[][] {
            {-2, -1, 0},
            {-1, 1, 1},
            { 0, 1, 2}
    };
    private static final double UNSHARP_MASKING_KERNEL[][] = new double[][] {
            {1, 4, 6, 4, 1},
            {4, 16, 24, 16, 4},
            {6, 24, -476, 24, 6},
            {4, 16, 24, 16, 4},
            {1, 4, 6, 4, 1}
    };
    
    @FXML
    private GridPane filterPane;

    @FXML
    private TextField divisorField;

    @FXML
    private ChoiceBox<String> edgeChooser;

    @FXML
    private Button applyButton;

    private TextField kernelField[][] = new TextField[KERNEL_SIZE][KERNEL_SIZE];

    @FXML
    private void handleIdentity(ActionEvent event){
        setKernel(IDENTITY_KERNEL);
    }

    @FXML
    private void handleBlur(ActionEvent event){
        setKernel(BLUR_KERNEL);
    }

    @FXML
    private void handleSharpen(ActionEvent event){
        setKernel(SHARPEN_KERNEL);
    }

    @FXML
    private void handleEdge(ActionEvent event){
        setKernel(EDGE_KERNEL);
    }

    @FXML
    private void handleSmallGaussian(ActionEvent event){
        setKernel(GAUSSIAN_3X3_KERNEL);
    }

    @FXML
    private void handleLargeGaussian(ActionEvent event){
        setKernel(GAUSSIAN_5X5_KERNEL);
    }

    @FXML
    private void handleEmboss(ActionEvent event){
        setKernel(EMBOSS_KERNEL);
    }

    @FXML
    private void handleUnsharp(ActionEvent event){
        setKernel(UNSHARP_MASKING_KERNEL);
    }

    /**
     * Initialize function for the window. Creates the kernelFields and initializes the edgeChooser.
     * @param location ignored
     * @param resources ignored
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        restrictToNumeric(divisorField);
        for(int x = 0; x < KERNEL_SIZE; ++x){
            for(int y = 0; y < KERNEL_SIZE; ++y) {
                TextField newKernelField = new TextField();
                newKernelField.setText(x == KERNEL_SIZE / 2 && y == KERNEL_SIZE / 2 ? "1" : "0");
                restrictToNumeric(newKernelField);
                filterPane.add(newKernelField, x, y);
                kernelField[x][y] = newKernelField;
            }
        }

        edgeChooser.setItems(FXCollections.observableArrayList(
                "Crop", "Extend", "Wrap", "Mirror"
        ));
        edgeChooser.setValue("Extend");
    }

    /**
     * Accessor for apply button object
     * @return reference to apply button
     */
    public Button getApplyButton(){
        return applyButton;
    }

    /**
     * Method to get a task to apply the image kernel
     * @param imageContainer imageView containing the image to filter, and where the new image will
     *                       be stored
     * @return Task to apply the kernel
     */
    public Task<Void> applyKernel(ImageView imageContainer) {
        Task<Void> filterTask = null;
        Image initial = imageContainer.getImage();
        if (initial == null) {
            throw new NullPointerException("No image to apply kernel to");
        }
        double kernel[][] = getKernel();
        double sum = 0;
        for (int x = 0; x < kernel.length; ++x) {
            for (int y = 0; y < kernel[x].length; ++y) {
                sum += kernel[x][y];
            }
        }
        if (sum >= 0) {
            filterTask = new Task<Void>() {
                @Override
                protected Void call() throws Exception {
                    int width = (int) initial.getWidth();
                    int height = (int) initial.getHeight();
                    WritableImage result = new WritableImage(width, height);
                    PixelWriter resultWriter = result.getPixelWriter();
                    for (int x = 0; x < width && !isCancelled(); ++x) {
                        for (int y = 0; y < height && !isCancelled(); ++y) {
                            resultWriter.setColor(x, y, sampleImage(x, y, initial));
                            updateProgress(x * height + y, width * height);
                        }
                    }
                    if (!isCancelled()) {
                        Platform.runLater(() -> imageContainer.setImage(result));
                    }
                    return null;
                }
            };
        }

        return filterTask;
    }


    /**
     * Reads the cells and returns a kernel as a 2d double array
     * @return double array of the values entered
     */
    private double[][] getKernel(){
        double kernel[][] = new double[KERNEL_SIZE][KERNEL_SIZE];
        for(int x = 0; x < KERNEL_SIZE; ++x){
            for(int y = 0; y < KERNEL_SIZE; ++y){
                double divisor = Double.parseDouble(divisorField.getText());
                double value = Double.parseDouble(kernelField[x][y].getText());
                if(divisor != 0){
                    value /= divisor;
                }
                kernel[x][y] = value;
            }
        }
        return kernel;
    }

    /**
     * Sets all of the cells to the value from kernel. If kernel is smaller than the maximum size,
     * the new kernel is centered in the main kernel. Also sets the divisor to the sum of the kernel
     * @param kernel kernel to set to
     */
    private void setKernel(double[][] kernel) {
        if (kernel.length != kernel[0].length) {
            throw new IllegalArgumentException("Kernel size does not match");
        }

        if(KERNEL_SIZE != kernel.length){
            for (int x = 0; x < KERNEL_SIZE; ++x) {
                for (int y = 0; y < KERNEL_SIZE; ++y) {
                    kernelField[x][y].setText("0.0");
                }
            }
        }

        int offset = (KERNEL_SIZE - kernel.length) / 2;
        for (int x = 0; x < kernel.length; ++x) {
            for (int y = 0; y < kernel.length; ++y) {

                kernelField[x + offset][y + offset].setText(Double.toString(kernel[x][y]));
            }
        }

        double sum = 0;
        for(int x = 0; x < kernel.length; ++x){
            for(int y = 0; y < kernel.length; ++y){
                sum += kernel[x][y];
            }
        }
        divisorField.setText(Double.toString(sum));
    }

    /**
     * Method to sample a pixel using the kernel.
     * @param x x coord to replace
     * @param y y coord to replace
     * @param image input image
     * @return the new Color after sampling nearby pixels using the kernel
     */
    private Color sampleImage(int x, int y, Image image){
        double kernel[][] = getKernel();
        int width = (int)image.getWidth();
        int height = (int)image.getHeight();
        PixelReader reader = image.getPixelReader();
        String edgeMethod = edgeChooser.getValue();
        double red = 0;
        double green = 0;
        double blue = 0;
        double alpha = 0;
        for(int row = 0; row < KERNEL_SIZE; ++row){
            for(int column = 0; column < KERNEL_SIZE; ++column){
                int convolutionBase = KERNEL_SIZE - 1;
                if(kernel[convolutionBase - column][convolutionBase - row] != 0) {
                    int offset = KERNEL_SIZE / 2;
                    int imageX = x + column - offset;
                    int imageY = y + row - offset;
                    boolean useValue = true;
                    if (imageX < 0) {
                        switch (edgeMethod) {
                            case "Crop":
                                useValue = false;
                                break;
                            case "Wrap":
                                imageX += width;
                                break;
                            case "Mirror":
                                imageX *= -1;
                                break;
                            case "Extend":
                                imageX = 0;
                                break;
                        }
                    } else if (imageX >= width) {
                        switch (edgeMethod) {
                            case "Crop":
                                useValue = false;
                                break;
                            case "Wrap":
                                imageX -= width;
                                break;
                            case "Mirror":
                                imageX = 2 * (width - 1) - imageX;
                                break;
                            case "Extend":
                                imageX = width - 1;
                                break;
                        }
                    }
                    if (imageY < 0) {
                        switch (edgeMethod) {
                            case "Crop":
                                useValue = false;
                                break;
                            case "Wrap":
                                imageY += height;
                                break;
                            case "Mirror":
                                imageY *= -1;
                                break;
                            case "Extend":
                                imageY = 0;
                                break;
                        }
                    } else if (imageY >= height) {
                        switch (edgeMethod) {
                            case "Crop":
                                useValue = false;
                                break;
                            case "Wrap":
                                imageY -= height;
                                break;
                            case "Mirror":
                                imageY = 2 * (height - 1) - imageY;
                                break;
                            case "Extend":
                                imageY = height - 1;
                                break;
                        }
                    }

                    if (useValue) {
                        Color color = reader.getColor(imageX, imageY);
                        red += kernel[convolutionBase - column][convolutionBase - row]
                                * color.getRed();
                        green += kernel[convolutionBase - column][convolutionBase - row]
                                * color.getGreen();
                        blue += kernel[convolutionBase - column][convolutionBase - row]
                                * color.getBlue();
                        alpha += kernel[convolutionBase - column][convolutionBase - row]
                                * color.getOpacity();
                    }
                }
            }
        }
        red = red > 1 ? 1 : red < 0 ? 0 : red;
        green = green > 1 ? 1 : green < 0 ? 0 : green;
        blue = blue > 1 ? 1 : blue < 0 ? 0 : blue;
        alpha = alpha > 1 ? 1 : alpha < 0 ? 0 : alpha;
        return new Color(red, green, blue, alpha);
    }

    /**
     * Method to restrict the input text field to only entering numeric values, all non-numeric
     * values are either ignored or numbers are automatically entered to make them valid
     * e.g. if '-' is entered, a '-1' shows up in the box, with 1 selected for easy replacement
     * @param field filed to restrict
     */
    private void restrictToNumeric(TextField field){
        field.textProperty().addListener(
                (ObservableValue<? extends String> observable, String oldValue,
                 String newValue) -> {
                    if(newValue.equals("")){
                        field.setText("0");
                        Platform.runLater(() -> {
                            field.positionCaret(0);
                            field.selectPositionCaret(1);
                        });
                    } else if (newValue.equals("-")){
                        field.setText("-1");
                        Platform.runLater(() -> {
                            field.positionCaret(newValue.length());
                            field.selectPositionCaret(newValue.length() + 1);
                        });
                    } else if (newValue.matches("-?\\d+\\.") && oldValue.matches("-?\\d+")) {
                        field.setText(newValue + '0');
                        Platform.runLater(() -> {
                            field.positionCaret(newValue.length());
                            field.selectPositionCaret(newValue.length() + 1);
                        });
                    } else if (!newValue.matches("-?\\d+(\\.)?(\\d+)?")) {
                        field.setText(oldValue);
                    }
                });
    }
}
