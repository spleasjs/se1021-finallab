/*/
Course: SE-1021
Term: Winter 2017-2018
Assignment: Final Lab
Author: Joshua Spleas
Date: 2/6/2018
/*/
package spleasjs;

import javafx.scene.paint.Color;

/**
 * Interface used for transformations on images
 */
public interface Transformable {
    /**
     * Method that stores the transformation code
     * @param x x coordinate of the input color
     * @param y y coordinate of the input color
     * @param colorIn input color
     * @return the modified color
     */
    Color transform(int x, int y, Color colorIn);
}
