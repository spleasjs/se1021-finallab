/*
 * Course: SE-1021
 * Term: Winter 2017-2018
 * Assignment: Final Lab
 * Author: Joshua Spleas
 * Date: 2/6/2018
 */
package spleasjs;

import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.input.KeyCode;
import javafx.scene.input.ScrollEvent;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.EOFException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * FXML Controller for the main window
 * Handles the menus and controls the image.
 */
public class MainController implements Initializable {
    /**
     * Width of the kernel filter window in pixels
     */
    private static final int FILTER_WIDTH = 300;
    /**
     * Height of the kernel filter window in pixels
     */
    private static final int FILTER_HEIGHT = 190;
    private static final int FILTER_BASE_POS = 50;
    private static final double ALMOST_ONE = .99;
    private static final double PERCENT_TO_DEC = .01;
    private static final int DEC_TO_PERCENT = 100;

    @FXML
    private ScrollPane imagePane;

    @FXML
    private ImageView imageView;

    @FXML
    private Menu fileMenu;

    @FXML
    private Menu transformMenu;

    @FXML
    private MenuItem filterMenu;

    @FXML
    private ProgressBar mainProgressBar;

    @FXML
    private Button cancelButton;

    @FXML
    private Label zoomLabel;

    private Task<Void> currentTask = null;
    private boolean ctrlDown = false;
    private Stage filterStage;
    private FilterController filterController;
    private File lastOpened = null;
    private PrintWriter logWriter = null;

    /**
     * Handles when show/hide filter window is pressed. Is also called when the user closes the
     * filter window.
     * @param e ignored
     */
    @FXML
    private void handleToggleFilter(ActionEvent e){
        if(filterStage.isShowing()){
            filterMenu.setText("Show Filter");
            filterStage.hide();
        } else {
            filterMenu.setText("Hide Filter");
            filterStage.show();
        }
    }

    /**
     * Handles when the user presses open
     * @param event ignored
     */
    @FXML
    private void handleOpen(ActionEvent event) {
        File file = getStandardChooser("Select a supported image file").showOpenDialog(
                imagePane.getScene().getWindow());
        openFromFile(file);
    }

    /**
     * Handles when reload is pressed, loads the last successfully loaded image
     * @param event ignored
     */
    @FXML
    private void handleReload(ActionEvent event){
        if(lastOpened != null){
            openFromFile(lastOpened);
        } else {
            logAndAlert("Loading Error", "No file has been opened!",
                    "Attempted to reload without an image loaded");
        }
    }

    /**
     * Saves the current image to the last successfully loaded file
     * @param event ignored
     */
    @FXML
    private void handleSave(ActionEvent event){
        if(imageView.getImage() != null){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Save File");
            alert.setHeaderText("Confirm Overwrite");
            alert.setContentText("This will overwite " + lastOpened.getName() + ". Is this OK?");
            ButtonType yes = new ButtonType("Yes");
            ButtonType no = new ButtonType("No");
            alert.getButtonTypes().clear();
            alert.getButtonTypes().addAll(yes, no);

            Optional<ButtonType> choice = alert.showAndWait();
            if(choice.get() == yes) {
                saveToFile(imageView.getImage(), lastOpened);
            }
        } else {
            logAndAlert("Image Error", "No image to save!",
                    "Attempted to save without having an image");
        }
    }

    /**
     * Handle when the user selects save as. Allows the user to save a file as a different type
     * @param event ignored
     */
    @FXML
    private void handleSaveAs(ActionEvent event){
        if(imageView.getImage() != null){

            File file = getStandardChooser("Save File")
                    .showSaveDialog(imagePane.getScene().getWindow());
            if(file != null) {
                saveToFile(imageView.getImage(), file);
            }
        } else {
            logAndAlert("Image Error", "No image to save!", "Attempted " +
                    "to save without having an image");
        }
    }

    /**
     * handles the grayscale transformation
     * @param event ignored
     */
    @FXML
    private void handleGray(ActionEvent event){
        Image in = imageView.getImage();
        if(in != null) {
            imageView.setImage(transformImage(imageView.getImage(), (x, y, color) ->
                    color.grayscale()));
        } else {
            logAndAlert("Image Error", "No image to transform!",
                    "Attempted to apply transformation without an image");
        }
    }

    /**
     * handles the negative transformation
     * @param event ignored
     */
    @FXML
    private void handleNegative(ActionEvent event){
        Image in = imageView.getImage();
        if(in != null) {
            imageView.setImage(transformImage(imageView.getImage(), (x, y, color) ->
                    color.invert()));
        } else {
            logAndAlert("Image Error", "No image to transform!",
                    "Attempted to apply transformation without an image");
        }
    }

    /**
     * handles the red transformation
     * @param event ignored
     */
    @FXML
    private void handleRed(ActionEvent event){
        Image in = imageView.getImage();
        if(in != null) {
            imageView.setImage(transformImage(imageView.getImage(), (x, y, color) ->
                    new Color(color.getRed(), 0, 0, color.getOpacity())));
        } else {
            logAndAlert("Image Error", "No image to transform!",
                    "Attempted to apply transformation without an image");
        }
    }

    /**
     * handles the red-gray transformation
     * @param event ignored
     */
    @FXML
    private void handleRedGray(ActionEvent event){
        Image in = imageView.getImage();
        if(in != null) {
            imageView.setImage(transformImage(imageView.getImage(), (x, y, color) -> {
                if(y % 2 == 0){
                    return color.grayscale();
                } else {
                    return new Color(color.getRed(), 0, 0, color.getOpacity());
                }
            }));
        } else {
            logAndAlert("Image Error", "No image to transform!",
                    "Attempted to apply transformation without an image");
        }
    }

    /**
     * Handler for cancel button, only active when a task is running
     * @param event ignored
     */
    @FXML
    private void handleCancel(ActionEvent event){
        if(currentTask != null){
            currentTask.cancel();
            mainProgressBar.progressProperty().unbind();
            mainProgressBar.progressProperty().set(0);
            toggleInterfaceActive(true);
        }
    }

    /**
     * Handler for any of the zoom buttons, text can be either 'Fit' of 'N%'
     * @param event what triggered the method, should be a button
     */
    @FXML
    private void handleZoomButton(ActionEvent event){
        if(event.getSource() instanceof Button) {
            Button source = (Button) event.getSource();
            String text = source.getText();
            if(text.equals("Fit")){
                zoomFit();
            } else if (text.matches("\\d+%")){
                zoomPercent(Integer.parseInt(text.substring(0, text.length() - 1)));
            }
        }
    }

    /**
     * initialize method, called as fxml is initialized, everything not main scene dependant is set
     * up here
     * @param location ignored
     * @param resources ignored
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mainProgressBar.progressProperty().set(0);
        imagePane.addEventFilter(ScrollEvent.SCROLL, (event -> {
            if(ctrlDown && imageView.getImage() != null){
                if(imageView.getFitHeight() != 0) {
                    imageView.setFitWidth(0);
                    if(imageView.getFitHeight() + event.getDeltaY() > 1) {
                        imageView.setFitHeight(imageView.getFitHeight() + event.getDeltaY());
                    } else{
                        imageView.setFitHeight(1);
                    }
                    updateZoomLabel();
                } else{
                    imageView.setFitHeight(0);
                    if(imageView.getFitWidth() + event.getDeltaY() > 1) {
                        imageView.setFitWidth(imageView.getFitWidth() + event.getDeltaY());
                    } else{
                        imageView.setFitWidth(1);
                    }
                    updateZoomLabel();
                }

                event.consume();
            }
        }));
    }

    /**
     * Method to initialize listeners for things that require the window to be set up completely
     * first e.g. any listeners for the scene
     */
    public void initializeSceneDependent(){
        FXMLLoader filterRootLoader = new FXMLLoader();
        filterStage = new Stage();
        filterStage.initOwner(imageView.getScene().getWindow());
        try {
            Parent filterRoot = filterRootLoader.load(getClass().
                    getResource("filter.fxml").openStream());
            filterStage.setTitle("Filter Kernel");
            filterStage.setResizable(false);
            filterStage.setScene(new Scene(filterRoot, FILTER_WIDTH, FILTER_HEIGHT));
        } catch (IOException e) {
            logAndAlert("FXML Error", "Unable to load filter fxml!",
                    "Unable to load filter.fxml");
        }
        filterStage.setOnCloseRequest((e) -> handleToggleFilter(new ActionEvent()));
        filterController = filterRootLoader.getController();
        Button filterApplyButton = filterController.getApplyButton();
        filterApplyButton.onActionProperty().setValue((event -> {
            if(imageView.getImage() != null) {
                currentTask = filterController.applyKernel(imageView);
                if (currentTask != null) {
                    startTask();
                    currentTask.setOnFailed((e) -> {
                        mainProgressBar.progressProperty().unbind();
                        mainProgressBar.progressProperty().set(0);
                        toggleInterfaceActive(true);
                        logAndAlert("Filter Error", "Unknown Error in filter!",
                                currentTask.getException().getMessage());
                        currentTask = null;
                    });
                } else {
                    logAndAlert("Kernel Error",
                            "Unable to apply kernel, check that the sum of elements is >= 0",
                            "Unable to generate kernel task, likely invalid kernel used");
                }
            } else {
                logAndAlert("Image Error", "No image to apply kernel to!",
                        "Attempted to apply kernel without an image");
            }
        }));

        Window mainWindow = imagePane.getScene().getWindow();
        Window filterWindow = filterStage.getScene().getWindow();
        mainWindow.yProperty().addListener((observable, oldValue, newValue) ->
                filterWindow.setY((int)((double)newValue + mainWindow.getHeight())));
        mainWindow.xProperty().addListener((observable, oldValue, newValue) ->
                filterWindow.setX((int)(double)newValue));
        ((Stage)mainWindow).maximizedProperty().addListener(((observable, oldValue, newValue) -> {
            if(newValue){
                filterWindow.setX(0);
                filterWindow.setY(FILTER_BASE_POS);
            }
        }));
        filterWindow.setX(mainWindow.getX());
        filterWindow.setY(mainWindow.getY() + mainWindow.getHeight());
        imagePane.getScene().setOnKeyPressed((event) -> {
            if(event.getCode() == KeyCode.CONTROL){
                ctrlDown = true;
            }
        });
        imagePane.getScene().setOnKeyReleased((event) -> {
            if(event.getCode() == KeyCode.CONTROL){
                ctrlDown = false;
            }
        });
        toggleInterfaceActive(true);
    }

    /**
     * Helper method to set up a file chooser with supported extensions
     * @param title Title of the window
     * @return the initialized FileChooser
     */
    private FileChooser getStandardChooser(String title){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle(title);
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Supported Images", "*.jpeg", "*.jpg", "*.gif",
                        "*.png", "*.bmp", "*.msoe", "*.bmsoe"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"),
                new FileChooser.ExtensionFilter("GIF", "*.gif"),
                new FileChooser.ExtensionFilter("MSOE", "*.msoe"),
                new FileChooser.ExtensionFilter("BMSOE", "*.bmsoe")
        );
        return fileChooser;
    }

    /**
     * Helper method to transform an image based on a Transformable function
     * @param inputImage image to apply the function with
     * @param transform function to determine new colors
     * @return transformed image
     */
    private Image transformImage(Image inputImage, Transformable transform){
        WritableImage outputImage = new WritableImage(inputImage.getPixelReader(),
                (int)inputImage.getWidth(), (int)inputImage.getHeight());
        PixelWriter writer = outputImage.getPixelWriter();
        PixelReader reader = outputImage.getPixelReader();
        for (int y = 0; y < outputImage.getHeight(); ++y) {
            for (int x = 0; x < outputImage.getWidth(); ++x) {
                writer.setColor(x, y, transform.transform(x, y, reader.getColor(x, y)));
            }
        }
        return outputImage;
    }

    /**
     * Sets the state of using the interface, used for tasks that run in the background
     * @param state true for active interface, false for active cancel button
     */
    private void toggleInterfaceActive(boolean state){
        fileMenu.disableProperty().setValue(!state);
        transformMenu.disableProperty().setValue(!state);
        filterController.getApplyButton().disableProperty().setValue(!state);
        cancelButton.disableProperty().setValue(state);
    }

    /**
     * zooms the image to fit in the pane
     */
    private void zoomFit() {
        Image image = imageView.getImage();
        if (image != null){
            if (image.getWidth() / imagePane.getWidth() >
                    image.getHeight() / imagePane.getHeight()){
                imageView.setFitHeight(0);
                imageView.setFitWidth(imagePane.getWidth() * ALMOST_ONE);
            } else {
                imageView.setFitWidth(0);
                imageView.setFitHeight(imagePane.getHeight() * ALMOST_ONE);
            }
            updateZoomLabel();
        }
    }

    /**
     * zooms the image to a specified percent
     * @param percent value in % to scale the image view to
     */
    private void zoomPercent(double percent){
        Image image = imageView.getImage();
        if (image != null) {
            imageView.setFitHeight(0);
            imageView.setFitWidth(PERCENT_TO_DEC * percent * image.getWidth());
            updateZoomLabel();
        }
    }

    /**
     * updates the zoom label to the current percent
     */
    private void updateZoomLabel() {
        Image image = imageView.getImage();
        if (image != null) {
            if (imageView.getFitHeight() != 0) {
                zoomLabel.setText("Zoom: " + new DecimalFormat("###.#")
                        .format(DEC_TO_PERCENT * imageView.getFitHeight() /
                                image.getHeight()) + "% ");
            } else {
                zoomLabel.setText("Zoom: " + new DecimalFormat("###.#")
                        .format(DEC_TO_PERCENT * imageView.getFitWidth() /
                                image.getWidth()) + "% ");
            }
        }
    }

    /**
     * Method to start the current task, binding the progress bar and toggling the interface state
     */
    private void startTask() {
        if(currentTask != null){
            mainProgressBar.progressProperty().set(0);
            mainProgressBar.progressProperty().bind(currentTask.progressProperty());
            toggleInterfaceActive(false);
            currentTask.setOnSucceeded((e) -> {
                mainProgressBar.progressProperty().unbind();
                mainProgressBar.progressProperty().set(0);
                toggleInterfaceActive(true);
                currentTask = null;
            });
            Thread thread = new Thread(currentTask);
            thread.setDaemon(true);
            thread.start();
        }
    }

    /**
     * Method to wrap the ImageIO reading and catch any exceptions
     * @param file file to read from
     */
    private void openFromFile(File file) {
        if (file != null) {
            try {
                currentTask = ImageIO.read(file, imageView);
                if(currentTask != null) {
                    startTask();
                    currentTask.setOnFailed((event) -> {
                        mainProgressBar.progressProperty().unbind();
                        mainProgressBar.progressProperty().set(0);
                        toggleInterfaceActive(true);
                        Throwable thrown = currentTask.getException();
                        if(thrown instanceof EOFException){
                            logAndAlert("File Read Error",
                                    "Unable to fully read input file, check for corruption",
                                    "End of file unexpectedly reached in : " + file.getPath());
                        } else if (thrown instanceof FileNotFoundException) {
                            //Should not happen if FileChooser is used
                            logAndAlert("File Read Error",
                                    "Unable to read input file, file was not found",
                                    "Unable to Find: " + file.getPath());
                        } else if (thrown instanceof NoSuchElementException) {
                            logAndAlert("File Read Error",
                                    "Unable to parse input file, check file formatting",
                                    "Unable to parse: " + file.getPath());
                        } else if (thrown instanceof IOException) {
                            logAndAlert("File Read Error",
                                    "General IOException when trying to load file",
                                    "General File Read Error: " + file.getPath());
                        } else{
                            logAndAlert("Reading Error",
                                    "Unknown Error in file reading!", thrown.getMessage());
                        }
                        currentTask = null;
                    });
                    currentTask.setOnSucceeded((e) -> {
                        mainProgressBar.progressProperty().unbind();
                        mainProgressBar.progressProperty().set(0);
                        toggleInterfaceActive(true);
                        currentTask = null;
                        zoomFit();
                        lastOpened = file;
                    });
                } else{
                    zoomFit();
                    lastOpened = file;
                }
            } catch (FileNotFoundException e) {
                //Should not happen if FileChooser is used
                logAndAlert("File Read Error", "Unable to read input file," +
                        " file was not found", "Unable to Find: " + file.getPath());
            } catch (EOFException e) {
                logAndAlert("File Read Error", "Unable to fully read input file, " +
                                "check for corruption",
                        "End of file unexpectedly reached in : " + file.getPath());
            } catch (IllegalArgumentException e){
                logAndAlert("File Read Error", e.getMessage(), e.getMessage());
            } catch (IOException e) {
                logAndAlert("File Read Error", "General IOException when " +
                        "trying to load file", "General File Read Error: " + file.getPath());
            }
        }
    }

    /**
     * Method to deal with saving the files and managing the task generated from ImageIO
     * @param image image to write
     * @param file file to write to
     */
    private void saveToFile(Image image, File file){
        if(file != null && image != null) {
            try {
                currentTask = ImageIO.write(image, file);
                if(currentTask != null){
                    startTask();
                    currentTask.setOnFailed((event) -> {
                        mainProgressBar.progressProperty().unbind();
                        mainProgressBar.progressProperty().set(0);
                        toggleInterfaceActive(true);
                        Throwable thrown = currentTask.getException();
                        if(thrown instanceof IOException){
                            logAndAlert("File Writing Error",
                                    "Unable image to write to file, check file type",
                                    "Unable to save to: " + file.getPath());
                        } else {
                            logAndAlert("Writing Error",
                                    "Unknown Error in file writing!", thrown.getMessage());
                        }
                        currentTask = null;
                    });
                } else{
                    lastOpened = file;
                }
            } catch (IOException e){
                logAndAlert("File Writing Error",
                        "Unable image to write to file, check file type",
                        "Unable to save to: " + file.getPath());
            }
        }
    }

    /**
     * Method to create a basic alert dialog for errors
     * @param header Header text to display
     * @param content Message to include on the alert
     */
    private void basicErrorAlert(String header, String content){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Error");
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    /**
     * Method that creates an alert and writes a string to the log file
     * @param header header for the alert and tag for the log
     * @param content content for the alert
     * @param log text to be written to the log
     */
    private void logAndAlert(String header, String content, String log){
        if(logWriter == null){
            try {
                logWriter = new PrintWriter("FinalLabLog.txt");
            } catch (IOException e){
                basicErrorAlert("Logging Error",
                        "Unable to Create Log File! Check permissions");
            }
        }
        if(logWriter != null){
            logWriter.println(header + ": " + log);
            logWriter.flush();
        }
        basicErrorAlert(header, content);
    }
}
